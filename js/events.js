var startTimes = {};

var exportToFile = (function () {
  var a = document.createElement("a");
  document.body.appendChild(a);
  a.style = "display: none";
  return function (data, fileName) {
    var json = JSON.stringify(data),
        blob = new Blob([json], {type: "application/json"}),
        url = window.URL.createObjectURL(blob);
    a.href = url;
    a.download = fileName;
    a.click();
    window.URL.revokeObjectURL(url);
  };
}());

function saveTime(startTime, endTime) {
  chrome.storage.local.get({ "session" }, function(result) {
    if (!chrome.runtime.error) {
      result.session.push({ "start_time": startTime, "end_time": endTime });
      chrome.storage.local.set({ "session": result.session });
    }
  });
}

chrome.extension.onMessage.addListener(function (request,sender,sendResponse) {
  var d = new Date();
  if (request.action === "export") {
    chrome.storage.local.get(null, function(result) {
      if (!chrome.runtime.error) {
        exportToFile(result, "export.json");
      }
    });
  } else if (request.action === "startTimer") {
    startTimes[sender.tab.id] = d.toString();
  } else if (request.action === "stopTimer") {
    saveTime(startTimes[sender.tab.id], d.toString());
    delete startTimes[sender.tab.id]
  }
});
