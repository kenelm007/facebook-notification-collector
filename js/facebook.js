/*
created by Shengjie Pan, jessepan@umich.edu, Jan 2017
*/

window.onload = (function checkNotificaiton() {
  
  var notifications = parseInt(document.getElementById('notificationsCountValue').innerHTML);

  date = new Date();

  saveNotification({
    "date": date.toString(),
    "new_notif": notifications
  });

  document.getElementById('fbNotificationsJewel').addEventListener('click', function(e) {
    e = e || window.event;
    
    unread = document.getElementsByClassName("jewelItemNew").length;

    notifType = "";
    notifContent = [];
    for (i = 0; i < e.path.length; i++) {
      if (e.path[i].nodeName && e.path[i].nodeName.toLowerCase() == 'li') {
        notifType = JSON.parse(e.path[i].getAttribute('data-gt')).notif_type;

        fwb = e.path[i].getElementsByClassName('fwb');
        for (j = 0; j < fwb.length; j++) {
          notifContent.push(fwb[j].textContent);
        }
        break;
      }
    }

    date = new Date();
    saveNotificationAction({
      "date": date.toString(),
      "unread_notif": unread,
      "notif_type": notifType,
      "notif_content": notifContent
    });
  }, false);

})();

function saveNotification(d) {
  chrome.storage.local.get({ "notification" : [] }, function(result) {
    if (!chrome.runtime.error) {
      result.notification.push(d);
      chrome.storage.local.set({ "notification": result.notification });
    }
  });
}

function saveNotificationAction(d) {
  chrome.storage.local.get({ "notification_action" : [] }, function(result) {
    if (!chrome.runtime.error) {
      result.notification_action.push(d);
      chrome.storage.local.set({ "notification_action": result.notification_action });
    }
  });
}

window.addEventListener('load',function(){
  chrome.extension.sendMessage({ action: "startTimer" }, function(response) {});
},false);

window.addEventListener('focus',function(){
  chrome.extension.sendMessage({ action: "startTimer" }, function(response) {});
},false);

window.addEventListener('blur',function(){
  chrome.extension.sendMessage({ action: "stopTimer" }, function(response) {});
},false);

window.addEventListener('beforeunload',function(){
  chrome.extension.sendMessage({ action: "stopTimer" }, function(response) {});
},false);
